/*******************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Aurora OS Tesla Model Demo project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

import DummyModels 1.0

Page {
    id: optionsPage
    allowedOrientations: Orientation.Portrait

    background: Rectangle {
        color: "#f0f3f7"
    }

    function add(accumulator, a) {
      return accumulator + a;
    }

    property real finalPrice
    property var selectedPrices: []

    Component.onCompleted: {
        for (var i = 0; i < tabsItem.count; i++) {
            selectedPrices.push(0)
        }
    }

    Item {
        id: pageHeader
        anchors.top: parent.top
        width: parent.width
        height: Theme.itemSizeHuge
        Rectangle {
            color: "white"
            anchors.fill: parent
        }

        Item {
            anchors.top: parent.top
            width: parent.width
            height: Theme.itemSizeMedium

            IconButton {
                id: backButton
                width: height
                height: Theme.iconSizeSmall
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: Theme.horizontalPageMargin
                icon.source: Qt.resolvedUrl("../resources/back-arrow.png")
                icon.color: "black"
                icon.highlightColor: "red"
                icon.width: backButton.width
                icon.height: backButton.height
                onClicked: {
                    if (tabsItem.currentIndex == 0) {
                        pageStack.replace(Qt.resolvedUrl("MainPage.qml"), null, PageStackAction.Immediate)
                        return
                    }
                    tabsItem.selectTab(tabsItem.currentIndex - 1)
                }
            }

            Image {
                source: Qt.resolvedUrl("../resources/tesla-logo-dark.png")
                anchors.centerIn: parent
                scale: 1.2
            }
        }

        Item {
            id: tabsItem
            anchors.bottom: parent.bottom
            width: parent.width
            height: Theme.itemSizeSmall

            property int currentIndex: 0
            property alias count: repeater.count

            function selectTab(selectedIndex) {
                for (var i = 0; i < repeater.count; i++) {
                    if (i === selectedIndex) {
                        repeater.itemAt(i).highlighted = true
                        continue
                    }
                    repeater.itemAt(i).highlighted = false
                }
                currentIndex = selectedIndex
            }

            Component.onCompleted: tabsItem.selectTab(0)

            Row {
                anchors.fill: parent

                Repeater {
                    id: repeater
                    model: DummyModels.optionsModel
                    anchors.bottom: parent.bottom

                    BackgroundItem {
                        id: tabItem
                        width: optionsPage.width / repeater.count
                        highlightedColor: "transparent"
                        enabled: (index <= tabsItem.currentIndex)

                        Rectangle {
                            width: parent.width
                            height: Theme.dp(4)
                            anchors.bottom: parent.bottom
                            color: highlighted ? "red" : "transparent"
                        }

                        Label {
                            text: (index + 1) + ". "+ name
                            anchors.centerIn: parent
                            color: "black"
                            font.bold: true
                            font.pixelSize: Theme.fontSizeTiny
                            opacity: tabItem.enabled ? 1 : 0.5
                        }

                        onClicked: {
                            tabsItem.selectTab(index)
                        }
                    }
                }
            }
        }

    }

    Loader {
        id: selectorLoader
        source: Qt.resolvedUrl("../selectors/" + DummyModels.optionsModel.get(tabsItem.currentIndex).filePrefix + "Selector.qml")
        anchors {
            left: parent.left
            right: parent.right
            top: pageHeader.bottom
            bottom: pageFooter.top
        }
        function priceRefresh(money) {
            var currentIndex = tabsItem.currentIndex
            optionsPage.selectedPrices.splice(currentIndex, 1, money)
            for (var i = currentIndex + 1; i < tabsItem.count; i++) {
                console.log(i, optionsPage.selectedPrices)
                optionsPage.selectedPrices.splice(i, 1, 0.0)
            }
            optionsPage.finalPrice = optionsPage.selectedPrices.reduce(optionsPage.add)
        }

        Connections {
            target: selectorLoader.item
            onMoneyChanged: {
                selectorLoader.priceRefresh(target.money)

            }
        }
        onSourceChanged: {
            priceRefresh(selectorLoader.item.money)
        }
    }

    Item {
        id: pageFooter
        anchors.bottom: parent.bottom
        width: parent.width
        height: Theme.itemSizeHuge

        Rectangle {
            anchors.top: parent.top
            color: "white"
            width: parent.width
            height: parent.height + radius
            radius: Theme.iconSizeSmall
        }

        Item {
            anchors.fill: parent
            anchors.margins: Theme.horizontalPageMargin * 4

            Label {
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                text: DummyModels.formatMoney(finalPrice)
                color: "black"
            }

            BackgroundItem {
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                width: parent.width / 2
                contentItem.radius: height
                contentItem.border.width: Theme.dp(2)
                contentItem.border.color: "red"
                highlightedColor: "red"

                Label {
                    text: qsTr("NEXT")
                    anchors.centerIn: parent
                    color: highlighted ? "white" : "black"
                }

                onClicked: {
                    if (tabsItem.currentIndex == tabsItem.count - 1) {
                        pageStack.replace(Qt.resolvedUrl("SummaryPage.qml"), {finalPrice: optionsPage.finalPrice})
                        return
                    }
                    tabsItem.selectTab(tabsItem.currentIndex + 1)
                }

            }
        }
    }
}
