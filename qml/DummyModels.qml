pragma Singleton
import QtQuick 2.0
import QtQml.Models 2.2

QtObject {

    function formatMoney(value) {
        if (value == 0) {
            return qsTr("Included")
        }
        return "$"+parseFloat(value).toLocaleString(Qt.locale("en_US"), 'f', 0)
    }

    property ListModel optionsModel: ListModel {
        ListElement { name: qsTr("Car"); filePrefix: "Car" }
        ListElement { name: qsTr("Exterior"); filePrefix: "Exterior"}
        ListElement { name: qsTr("Interior"); filePrefix: "Interior"}
        ListElement { name: qsTr("Autopilot"); filePrefix: "Autopilot"}
    }

    property ListModel carOptionsModel: ListModel {
        ListElement {
            name: qsTr("Performance")
            price: "55700"
            description: qsTr("Tesla All-Wheel Drive has two independent motors. Unlike traditional all-wheel drive systems, these two motors digitally control torque to the front and rear wheels—for far better handling and traction control.")
            topSpeed: qsTr("150mph")
            sixtyMilesTime: qsTr("3.5s")
        }

        ListElement {
            name: qsTr("Long Range")
            price: "46700"
            description: qsTr("Some boring Long Range description")
            topSpeed: qsTr("130mph")
            sixtyMilesTime: qsTr("5s")
        }
    }

    property ListModel exteriorColorsModel: ListModel {
        ListElement { color: "black"; name: qsTr("Fancy Black"); price: "2000"}
        ListElement { color: "grey"; name: qsTr("Space Gray"); price: "1700"}
        ListElement { color: "blue"; name: qsTr("Sad Blue"); price: "1800"}
        ListElement { color: "white"; name: qsTr("Pearl White Multi-Coat"); price: "1900"}
        ListElement { color: "red"; name: qsTr("Deafening Red"); price: "2000"}
    }

    property ListModel interiorColorsModel: ListModel {
        ListElement { color: "white"; name: qsTr("Black and White"); price: "1000"}
        ListElement { color: "black"; name: qsTr("All Black"); price: "0"}

    }

    property ListModel autopilotModel: ListModel {
        ListElement { name: qsTr("Full Self-Driving"); price: "5000"
            description: qsTr("Atomatic driving from highway on-ramp to off-ramp including interchanges and overtaking slower cars.")
        }
        ListElement { name: qsTr("Autopilot"); price: "3000"
            description: qsTr("Some boring autopilot description")
        }

    }

    property ListModel dummyModel: ListModel {
        ListElement {
            name: qsTr("Model X")
            range: qsTr("300 mi")
            motor: qsTr("AWD")
            acceleration: qsTr("0-60 mph in 3.5s")
            topSpeed: qsTr("up to 150 mph")
        }

        ListElement {
            name: qsTr("Model Y")
            range: qsTr("300 mi")
            motor: qsTr("AWD")
            acceleration: qsTr("0-60 mph in 3.5s")
            topSpeed: qsTr("up to 150 mph")
        }

        ListElement {
            name: qsTr("Roadster")
            range: qsTr("300 mi")
            motor: qsTr("AWD")
            acceleration: qsTr("0-60 mph in 3.5s")
            topSpeed: qsTr("up to 150 mph")
        }
    }
}
