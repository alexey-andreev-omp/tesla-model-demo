/*******************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Aurora OS Tesla Model Demo project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

import DummyModels 1.0

import "../components"

SelectorItem {
    id: autopilotSelector
    anchors.fill: parent

    Image {
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: selectorFooter.top
        }

        fillMode: Image.PreserveAspectCrop
        source: Qt.resolvedUrl("../resources/autopilot-bg.png")
    }

    Rectangle {
        id: selectorFooter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -radius
        width: parent.width
        height: Theme.itemSizeHuge * 1.5 + radius
        radius: Theme.iconSizeSmall
        Rectangle {
            width: parent.width
            height: parent.height / 2
            anchors.bottom: parent.bottom
        }
    }

    Item {
        width: parent.width
        height: Theme.itemSizeHuge * 1.5
        anchors.bottom: parent.bottom
        Item {
            anchors.fill: parent
            anchors.margins: Theme.paddingLarge

            Column {
                anchors.fill: parent

                Label {
                    text: qsTr("Autopilot")
                    font.pixelSize: Theme.fontSizeLarge
                    color: "#A4B0BC"
                }

                LabelsRow {
                    id: labelsRow
                    model: DummyModels.autopilotModel
                    onClickedSelect: {
                        autopilotSelector.money = modelGet(currentIndex).price
                    }
                }

                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: labelsRow.modelGet(labelsRow.currentIndex).description
                    font.pixelSize: Theme.fontSizeExtraSmall
                    color: "#A4B0BC"
                }
            }
        }
    }
    Component.onCompleted: {
        autopilotSelector.money = labelsRow.modelGet(labelsRow.currentIndex).price
    }
}

