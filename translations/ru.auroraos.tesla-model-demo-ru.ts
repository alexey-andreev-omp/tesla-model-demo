<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Проект предоставляет шаблон приложений для ОС&amp;nbsp;Аврора.&lt;/p&gt;
&lt;p&gt;Основная цель&amp;nbsp;— наглядно продемонстрировать практически минимальный исходный код для получения корректного и расширяемого приложения.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="83"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2022 Open Mobile Platform LLC&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
  &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
  &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
  &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>3-Clause BSD License</source>
        <translation>Лицензия 3-Clause BSD</translation>
    </message>
</context>
<context>
    <name>AutopilotSelector</name>
    <message>
        <location filename="../qml/selectors/AutopilotSelector.qml" line="88"/>
        <source>Autopilot</source>
        <translation>Автопилот</translation>
    </message>
</context>
<context>
    <name>CarSelector</name>
    <message>
        <location filename="../qml/selectors/CarSelector.qml" line="56"/>
        <source>Select your car</source>
        <translation>Выберите вашу машину</translation>
    </message>
    <message>
        <location filename="../qml/selectors/CarSelector.qml" line="112"/>
        <source>0-60 mph</source>
        <translation>0-60 миль/час</translation>
    </message>
    <message>
        <location filename="../qml/selectors/CarSelector.qml" line="133"/>
        <source>Top Speed</source>
        <translation>Максимальная скорость</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="47"/>
        <source>Tesla Model Demo</source>
        <translation>Демо Модели Тесла</translation>
    </message>
</context>
<context>
    <name>DummyModels</name>
    <message>
        <location filename="../qml/DummyModels.qml" line="9"/>
        <source>Included</source>
        <translation>Включено</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="15"/>
        <source>Car</source>
        <translation>Машина</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="16"/>
        <source>Exterior</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="17"/>
        <source>Interior</source>
        <translation>Интерьер</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="18"/>
        <location filename="../qml/DummyModels.qml" line="57"/>
        <source>Autopilot</source>
        <translation>Автопилот</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="23"/>
        <source>Performance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="25"/>
        <source>Tesla All-Wheel Drive has two independent motors. Unlike traditional all-wheel drive systems, these two motors digitally control torque to the front and rear wheels—for far better handling and traction control.</source>
        <translation>Tesla All-Wheel Drive имеет два независимых двигателя. В отличие от традиционных полноприводных систем, эти два двигателя в цифровой форме регулируют крутящий момент на передние и задние колеса, что значительно улучшает управляемость и контроль тяги.</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="26"/>
        <source>150mph</source>
        <translation>150 миль/час</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="27"/>
        <source>3.5s</source>
        <translation>3.5 сек</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="31"/>
        <source>Long Range</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="33"/>
        <source>Some boring Long Range description</source>
        <translation>Скучное описание для Long Range</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="34"/>
        <source>130mph</source>
        <translation>130 миль/час</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="35"/>
        <source>5s</source>
        <translation>5 сек</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="40"/>
        <source>Fancy Black</source>
        <translation>Модный черный</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="41"/>
        <source>Space Gray</source>
        <translation>Космический серый</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="42"/>
        <source>Sad Blue</source>
        <translation>Унылый синий</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="43"/>
        <source>Pearl White Multi-Coat</source>
        <translation>Многослойный жемчужно-белый</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="44"/>
        <source>Deafening Red</source>
        <translation>Оглушающий красный</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="48"/>
        <source>Black and White</source>
        <translation>Черно-белый</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="49"/>
        <source>All Black</source>
        <translation>Черный</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="54"/>
        <source>Full Self-Driving</source>
        <translation>Самоуправление</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="55"/>
        <source>Atomatic driving from highway on-ramp to off-ramp including interchanges and overtaking slower cars.</source>
        <translation>Автоматическое движение от въезда на съезд к съезду с шоссе, включая развязки и обгон более медленных автомобилей.</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="58"/>
        <source>Some boring autopilot description</source>
        <translation>Скучное описание автопилота</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="65"/>
        <source>Model X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="66"/>
        <location filename="../qml/DummyModels.qml" line="74"/>
        <location filename="../qml/DummyModels.qml" line="82"/>
        <source>300 mi</source>
        <translation>300 миль</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="67"/>
        <location filename="../qml/DummyModels.qml" line="75"/>
        <location filename="../qml/DummyModels.qml" line="83"/>
        <source>AWD</source>
        <translation>полный привод</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="68"/>
        <location filename="../qml/DummyModels.qml" line="76"/>
        <location filename="../qml/DummyModels.qml" line="84"/>
        <source>0-60 mph in 3.5s</source>
        <translation>до 60 миль за 3.5 сек</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="69"/>
        <location filename="../qml/DummyModels.qml" line="77"/>
        <location filename="../qml/DummyModels.qml" line="85"/>
        <source>up to 150 mph</source>
        <translation>до 150 миль в час</translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="73"/>
        <source>Model Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/DummyModels.qml" line="81"/>
        <source>Roadster</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ExteriorSelector</name>
    <message>
        <location filename="../qml/selectors/ExteriorSelector.qml" line="55"/>
        <source>Select color</source>
        <translation>Выберите цвет</translation>
    </message>
    <message>
        <location filename="../qml/selectors/ExteriorSelector.qml" line="107"/>
        <source>20’’ Performance Wheels</source>
        <translation>Performance Wheels 20 дюймов</translation>
    </message>
    <message>
        <location filename="../qml/selectors/ExteriorSelector.qml" line="113"/>
        <source>Carbon fiber spoiler</source>
        <translation>Спойлер из углеродного волокна</translation>
    </message>
</context>
<context>
    <name>InteriorSelector</name>
    <message>
        <location filename="../qml/selectors/InteriorSelector.qml" line="88"/>
        <source>Select interior</source>
        <translation>Выбрать интерьер</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="83"/>
        <source>ORDER NOW</source>
        <translation>ЗАКАЗАТЬ</translation>
    </message>
</context>
<context>
    <name>OptionsPage</name>
    <message>
        <location filename="../qml/pages/OptionsPage.qml" line="237"/>
        <source>NEXT</source>
        <translation>ДАЛЕЕ</translation>
    </message>
</context>
<context>
    <name>SummaryPage</name>
    <message>
        <location filename="../qml/pages/SummaryPage.qml" line="97"/>
        <source>Summary</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="../qml/pages/SummaryPage.qml" line="109"/>
        <source>Your </source>
        <translation>Ваша </translation>
    </message>
    <message>
        <location filename="../qml/pages/SummaryPage.qml" line="156"/>
        <source>Pay</source>
        <translation></translation>
    </message>
</context>
</TS>
